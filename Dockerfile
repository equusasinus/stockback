FROM openjdk:11.0.9-jre
COPY ./target/Test-0.0.1-SNAPSHOT.jar /home/
ENTRYPOINT ["java","-jar","/home/Test-0.0.1-SNAPSHOT.jar"]