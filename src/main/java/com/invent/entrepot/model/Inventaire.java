package com.invent.entrepot.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.invent.entrepot.util.KeyValueContainer;
import com.invent.entrepot.util.ProductUtil;

@Entity
@Table(name = "inventaire")
public class Inventaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4075651745757310070L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_inventaire")
	private Long id_inventaire;

	@Column(name = "num_inventaire")
	private String num_inventaire;

	@Column(name = "date_inventaire")
	private Date date_inventaire;

	public Long getId_inventaire() {
		return id_inventaire;
	}

	public void setId_inventaire(Long id_inventaire) {
		this.id_inventaire = id_inventaire;
	}

	public String getNum_inventaire() {
		return num_inventaire;
	}

	public void setNum_inventaire(String num_inventaire) {
		this.num_inventaire = num_inventaire;
	}

	public Date getDate_inventaire() {
		return date_inventaire;
	}

	public void setDate_inventaire(Date date_inventaire) {
		this.date_inventaire = date_inventaire;
	}

	public boolean isClose() {
		return close;
	}

	public void setClose(boolean close) {
		this.close = close;
	}

	@ElementCollection
	@CollectionTable(name = "produit_inventaire", joinColumns = @JoinColumn(name = "id_inventaire"))
	@MapKeyJoinColumn(name = "id")
	@Column(name = "quantity")
	private Map<Produit, Long> produits;

	@Override
	public String toString() {
		return "Inventaire (id_inventaire=" + id_inventaire + ", num_inventaire=" + num_inventaire
				+ ", date_inventaire=" + date_inventaire + ", produit=" + produits + ", close=" + close + ")";
	}

	@Column(name = "close")
	private boolean close;

	public Inventaire() {

	}

	@JsonProperty("produits")
	public List<KeyValueContainer<Produit, Long>> getProducts() {
		return ProductUtil.toList(produits);
	}

	@JsonProperty("produits")
	public void setProductsList(List<KeyValueContainer<Produit, Long>> list) {
		produits = ProductUtil.toMap(list);
	}

}
