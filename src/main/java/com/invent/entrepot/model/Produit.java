package com.invent.entrepot.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produit")
public class Produit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3582051536027579904L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long idProduit;

	@Column(name = "code_produit")
	private String codeProduit;

	@Column(name = "designation")
	private String designation;

	@Column(name = "prix_UnitaireHT")
	private BigDecimal prixUnitaireHT;

	@Column(name = "categorie")
	private String categorie;

	@Column(name = "quantite")
	private Long quantite;

	public Long getIdProduit() {
		return idProduit;
	}

	public Long getQuantite() {
		return quantite;
	}

	public void setQuantite(Long quantite) {
		this.quantite = quantite;
	}

	public void setIdProduit(Long idProduit) {
		this.idProduit = idProduit;
	}

	public String getCodeProduit() {
		return codeProduit;
	}

	public void setCodeProduit(String codeProduit) {
		this.codeProduit = codeProduit;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public Produit() {
	}

	@Override
	public String toString() {
		return "Produit (idProduit=" + idProduit + ", codeProduit=" + codeProduit + ", designation=" + designation
				+ ", prixUnitaireHT=" + prixUnitaireHT + ", categorie=" + categorie + ", quantite=" + quantite + ")";
	}

}
