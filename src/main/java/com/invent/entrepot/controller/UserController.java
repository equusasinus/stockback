package com.invent.entrepot.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.invent.entrepot.model.User;
import com.invent.entrepot.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/users")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<User>> getUsers() {
		List<User> users = userService.findAll();
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	/*
	 * a revoir vers la fin si on garde ou pas
	 */
	@GetMapping(value = "/getUser")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<User> getUser(Principal principal) {
		User user = userService.getUserByEmail(principal.getName());
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/*
	 * Modification d'un compte utilisateur avec son identifiat=nt
	 */

	@PutMapping("/update/{userId}")
	@PreAuthorize("hasRole('ADMIN')")
	public User updateUser(@PathVariable(value = "userId") Long userId, @Valid @RequestBody User userRequest) {
		return userService.updateUser(userId, userRequest);
	}

	/*
	 * Suppresion d'un compte utilisateur avec son identifiant
	 * 
	 */

	@DeleteMapping("/delete/{userId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteUser(@PathVariable(value = "userId") Long userId) {
		return userService.deleteUser(userId);
	}

	@PostMapping(value = "/updateUserInfo")
	public ResponseEntity profileInfo(@RequestBody HashMap<String, Object> mapper) throws Exception {

		Long id = (Long) mapper.get("id");
		String email = (String) mapper.get("email");
		String firstName = (String) mapper.get("firstName");
		String lastName = (String) mapper.get("lastName");
		String newPassword = (String) mapper.get("newPassword");
		String currentPassword = (String) mapper.get("currentPassword");

		User currentUser = userService.findById(id);

		if (currentUser == null) {
			throw new Exception("User not found");
		}

		if (userService.getUserByEmail(email) != null) {
			if (userService.getUserByEmail(email).getId() != currentUser.getId()) {
				return new ResponseEntity("Email not found!", HttpStatus.BAD_REQUEST);
			}
		}


		currentUser.setFirstName(firstName);
		currentUser.setLastName(lastName);
		
		userService.save(currentUser);

		return new ResponseEntity("update sucess",HttpStatus.OK);

	}
}
