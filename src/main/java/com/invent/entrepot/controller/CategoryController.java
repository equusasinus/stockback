
package com.invent.entrepot.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.invent.entrepot.domain.Response;
import com.invent.entrepot.model.Category;
import com.invent.entrepot.service.CategoryService;

@RestController
public class CategoryController {

	@Autowired
	CategoryService categoryService;

	@PostMapping(value = "/category/enregistrer")
	public ResponseEntity<Response> registration(@RequestBody Category category) {
		Category dbCategory = categoryService.save(category);
		if (dbCategory != null) {
			return new ResponseEntity<>(new Response("Categorie is saved successfuly"), HttpStatus.OK);

		}
		return null;

	}

	@GetMapping(value = "/category/listes")
	//@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<Category>> getCategry() {
		List<Category> category = categoryService.findAll();
		return new ResponseEntity<List<Category>>(category, HttpStatus.OK);
	}

	@PutMapping("/update/category/{categoId}")
	public Category updateCategory(@PathVariable(value = "categoId") Long categoId,
			@Valid @RequestBody Category categoryRequest) {
		return categoryService.updateCategory(categoId, categoryRequest);
	}

	@DeleteMapping("/delete/category/{categoId}")
	public ResponseEntity<?> deleteCategory(@PathVariable(value = "categoId") Long categoId) {
		return categoryService.deleteCategory(categoId);

	}

}
