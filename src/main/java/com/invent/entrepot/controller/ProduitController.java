package com.invent.entrepot.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.invent.entrepot.domain.Response;
import com.invent.entrepot.model.Produit;
import com.invent.entrepot.service.ProduitService;

@RestController
public class ProduitController {

	@Autowired
	ProduitService produitService;

	/*
	 * Enregistrer un produit
	 * 
	 */

	@PostMapping(value = "/produit/enregistrer")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Response> registration(@RequestBody Produit produit) {
		Produit dbProduit = produitService.save(produit);
		if (dbProduit != null) {
			return new ResponseEntity<>(new Response("Produit is saved successfully"), HttpStatus.OK);
		}

		return null;

	}

	/*
	 * Liste des produits
	 * 
	 */

	@GetMapping(value = "/produit/listes")
	//@PreAuthorize("hasRole('ADMIN') ")
	public ResponseEntity<List<Produit>> getProduit() {
		List<Produit> produit = produitService.findAll();
		return new ResponseEntity<List<Produit>>(produit, HttpStatus.OK);
	}

	/*
	 * Modifier un produit or hasRole('GESTIONNAIRE')
	 * 
	 */

	@PutMapping("/produit/update/{produiId}")
	@PreAuthorize("hasRole('ADMIN')")
	public Produit updateProduit(@PathVariable(value = "produiId") Long produiId,
			@Valid @RequestBody Produit produitRequest) {
		return produitService.updateProduit(produiId, produitRequest);
	}

	/*
	 * Supprimer un produit
	 * 
	 * }
	 * 
	 */
	@DeleteMapping("/delete/produit/{produiId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteProduit(@PathVariable(value = "produiId") Long produiId) {
		return produitService.deleteProduit(produiId);

	}

}
