package com.invent.entrepot.controller;


import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.invent.entrepot.domain.Response;
import com.invent.entrepot.model.Inventaire;
import com.invent.entrepot.service.InventaireService;
import com.invent.entrepot.service.ProduitService;

@RestController
public class InventaireController {

	@Autowired
	InventaireService inventaireService;

	@Autowired
	ProduitService productService;

	@PostMapping(value = "/inventaire/enregistrer")
	public ResponseEntity<Response> registration(@RequestBody Inventaire inventaire) {
		Inventaire dbInventaire = inventaireService.save(inventaire);
		if (dbInventaire != null) {
			return new ResponseEntity<>(new Response("Inventaire is saved successfully"), HttpStatus.OK);
		  
		}
		return null;

	}


	@GetMapping(value = "/inventaire/listes")
	// @PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<Inventaire>> getInventaire() {
		List<Inventaire> inventaire = inventaireService.findAll();
		return new ResponseEntity<List<Inventaire>>(inventaire, HttpStatus.OK);
	}

	@PutMapping("/inventaire/update/{inventaireId}")
	public Inventaire updateInventaire(@PathVariable(value = "inventaireId") Long inventaireId,
			@Valid @RequestBody Inventaire inventaireRequest) {
		return inventaireService.updateInventaire(inventaireId, inventaireRequest);
	}

	@DeleteMapping("/inventaire/delete/{inventaireId}")
	public ResponseEntity<?> deleteInventaire(@PathVariable(value = "inventaireId") Long inventaireId) {
		return inventaireService.deleteInventaire(inventaireId);

	}

}
