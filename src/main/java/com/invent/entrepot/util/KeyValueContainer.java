package com.invent.entrepot.util;

public class KeyValueContainer<K,V> {

	public KeyValueContainer() {
	}

	public KeyValueContainer(K k, V v) {
		this.key = k;
		this.value = v;

	}

	private K key;
	private V value;
	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

}
