package com.invent.entrepot.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;


import com.invent.entrepot.model.Inventaire;

@RepositoryRestController
public interface InventaireRepository extends JpaRepository<Inventaire, Long> {

}
