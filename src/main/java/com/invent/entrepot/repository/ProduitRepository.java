package com.invent.entrepot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.stereotype.Repository;

import com.invent.entrepot.model.Produit;

@RepositoryRestController
public interface ProduitRepository extends JpaRepository<Produit, Long> {

	
}
