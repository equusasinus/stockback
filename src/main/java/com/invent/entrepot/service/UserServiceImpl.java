package com.invent.entrepot.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invent.entrepot.exception.ResourceNotFoundException;
import com.invent.entrepot.model.User;
import com.invent.entrepot.repository.UserRepository;
import com.invent.entrepot.util.PasswordUtil;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User save(User user) {
		String password = PasswordUtil.getPasswordHash(user.getPassword());
		user.setPassword(password);
		user.setCreatedDate(new Date());
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User getUserByEmail(String email) {
		return userRepository.findByEmailIgnoreCase(email);
	}

	@Override
	public User updateUser(Long userId, User userRequest) {
		
		return userRepository.findById(userId).map(user -> {
			String password = PasswordUtil.getPasswordHash(userRequest.getPassword());
			user.setLastName(userRequest.getLastName());
			user.setFirstName(userRequest.getFirstName());
			user.setEmail(userRequest.getEmail());
			user.setPassword(password);
			user.setPhoneNumber(userRequest.getPhoneNumber());
			user.setRole(userRequest.getRole());
			user.setUpdatedDate(new Date());
			return userRepository.save(user);
		}).orElseThrow(() -> new ResourceNotFoundException("userId " + userId + "not found"));
	}

	@Override
	public ResponseEntity<?> deleteUser(Long userId) {
		return userRepository.findById(userId).map(user -> {
			userRepository.delete(user);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("userId " + userId + " not found"));
	}

	@Override
	public User findById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.getOne(id);
	}

}
