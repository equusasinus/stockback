package com.invent.entrepot.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.invent.entrepot.model.Produit;


public interface ProduitService {

	List<Produit> findAll();

	Produit save(Produit produit);

	Produit updateProduit(Long produiId,Produit produit);

	ResponseEntity<?> deleteProduit(Long produiId);



}
