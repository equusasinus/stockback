package com.invent.entrepot.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.invent.entrepot.model.Inventaire;

public interface InventaireService {

	Inventaire save(Inventaire inventaire);

	List<Inventaire> findAll();

	Inventaire updateInventaire(Long inventaireId, Inventaire inventaire);

	ResponseEntity<?> deleteInventaire(Long inventaireId);

}
