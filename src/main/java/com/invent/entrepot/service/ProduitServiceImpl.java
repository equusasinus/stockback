package com.invent.entrepot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invent.entrepot.exception.ResourceNotFoundException;
import com.invent.entrepot.model.Produit;
import com.invent.entrepot.repository.ProduitRepository;

@Service
@Transactional
public class ProduitServiceImpl implements ProduitService {
	@Autowired
	private ProduitRepository produitRepository;

	@Override
	public List<Produit> findAll() {
		return (List<Produit>) produitRepository.findAll();
	}

	@Override
	public Produit save(Produit produit) {
		return produitRepository.save(produit);
	}

	@Override
	public Produit updateProduit(Long produiId, Produit produitRequest) {

		return produitRepository.findById(produiId).map(produit -> {
			produit.setCodeProduit(produitRequest.getCodeProduit());
			produit.setCategorie(produitRequest.getCategorie());
			produit.setDesignation(produitRequest.getDesignation());
			produit.setPrixUnitaireHT(produitRequest.getPrixUnitaireHT());
			produit.setQuantite(produitRequest.getQuantite());
			return produitRepository.save(produit);
		}).orElseThrow(() -> new ResourceNotFoundException("produiId " + produiId + "not found"));

	}

	@Override
	public ResponseEntity<?> deleteProduit(Long produiId) {
		return produitRepository.findById(produiId).map(produit -> {
			produitRepository.delete(produit);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("produiId " + produiId + " not found"));
	}

	

}
