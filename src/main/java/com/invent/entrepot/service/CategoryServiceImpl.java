package com.invent.entrepot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invent.entrepot.exception.ResourceNotFoundException;
import com.invent.entrepot.model.Category;
import com.invent.entrepot.repository.CategoryRepository;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category save(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public List<Category> findAll() {
		return (List<Category>) categoryRepository.findAll();
	}

	@Override
	public Category updateCategory(Long categoId, Category categoryRequest) {
		return categoryRepository.findById(categoId).map(category -> {
			category.setDesignation(categoryRequest.getDesignation());
			category.setCode(categoryRequest.getCode());
			return categoryRepository.save(category);
		}).orElseThrow(() -> new ResourceNotFoundException("categoId " + categoId + "not found"));

	}

	@Override
	public ResponseEntity<?> deleteCategory(Long categoId) {
		return categoryRepository.findById(categoId).map(category -> {
			categoryRepository.delete(category);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("categoId " + categoId + " not found"));
	}

}
