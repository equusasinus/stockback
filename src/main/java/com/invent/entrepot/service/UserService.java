package com.invent.entrepot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.invent.entrepot.model.User;

public interface UserService {

	User save(User user);

	List<User> findAll();

	User getUserByEmail(String email);

	User updateUser(Long userId, User user);

	ResponseEntity<?> deleteUser(Long userId);

	User findById(Long id);


	

}
