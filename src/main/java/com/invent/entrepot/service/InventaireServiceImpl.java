package com.invent.entrepot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.invent.entrepot.exception.ResourceNotFoundException;
import com.invent.entrepot.model.Inventaire;
import com.invent.entrepot.repository.InventaireRepository;

@Service
@Transactional
public class InventaireServiceImpl implements InventaireService {

	@Autowired
	InventaireRepository inventaireRepository;

	@Override
	public Inventaire save(Inventaire inventaire) {
		return inventaireRepository.save(inventaire);
	}

	@Override
	public List<Inventaire> findAll() {
		return inventaireRepository.findAll();
	}

	@Override
	public Inventaire updateInventaire(Long inventaireId, Inventaire inventaireRequest) {
		return inventaireRepository.findById(inventaireId).map(inventaire -> {
			inventaire.setDate_inventaire(inventaireRequest.getDate_inventaire());
			inventaire.setNum_inventaire(inventaireRequest.getNum_inventaire());
			inventaire.setProductsList(inventaireRequest.getProducts());
			
			return inventaireRepository.save(inventaire);
		}).orElseThrow(() -> new ResourceNotFoundException("inventaireId " + inventaireId + "not found"));

	}
	
	
	@Override
	public ResponseEntity<?> deleteInventaire(Long inventaireId) {
		return inventaireRepository.findById(inventaireId).map(inventaire -> {
			inventaireRepository.delete(inventaire);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("inventaireId " + inventaireId + " not found"));
	}

	
	
	
}
