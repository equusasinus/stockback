package com.invent.entrepot.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.invent.entrepot.model.Category;

public interface CategoryService {

	Category save(Category category);

	List<Category> findAll();

	Category updateCategory(Long categoId, Category category);

	ResponseEntity<?> deleteCategory(Long categoId);

}
